class ResourcesController < ApplicationController

  def index
    @api = Specification.instance.api
  end

  def show
    @base_url = Specification.instance.api[:base_url]
    @service = Specification.instance.service params[:method], params[:resource]
  end

end
