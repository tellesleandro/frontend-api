class Specification

  include Singleton

  def api
    return @api unless @api.nil?
    api_specification_file = Rails.configuration.api_specification_file
    return nil unless File.exist?(api_specification_file)
    specification = YAML.load_file(api_specification_file)
    @api ||= {
      title: "#{specification['info']['title']} - versão #{specification['info']['version']}",
      description: "#{specification['info']['description']}",
      base_url: "#{specification['schemes'][0]}://#{specification['host']}#{specification['basePath']}",
      services: services(specification['paths'])
    }
  end

  def service(method, resource)
    @api[:services].find{|service| service[:method] == method && service[:resource] == resource}
  end

  private

  def services(paths)
    retorno = []
    paths.each do |resource, resource_value|
      resource_value.each do |method, method_value|
        retorno << {
          method: method,
          resource: resource,
          description: method_value['description'],
          parameters: parameters(method_value['parameters'])
        }
      end
    end
    retorno
  end

  def parameters(parameters)
    retorno = []
    parameters.each do |parameter|
      retorno << {
        name: parameter['name'],
        description: parameter['description'],
        required: parameter['required'],
        type: parameter['type']
      }
    end
    retorno
  end

end
